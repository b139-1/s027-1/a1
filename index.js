const http = require("http");
const PORT = 4000;

const server = http.createServer((req, res) => {
	

	if(req.url === "/" && req.method === "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Welcome to the Booking System!`);
		res.end();
	} else if(req.url === "/profile" && req.method === "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Welcome to your profile!`);
		res.end();
	} else if(req.url === "/courses" && req.method === "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Here's our available courses`);
		res.end();
	} else if(req.url === "/addCourse" && req.method === "POST") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Add course to our resources`);
		res.end();
	} else if(req.url === "/updateCourse" && req.method === "PUT") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Update a course in our resources`);
		res.end();
	} else if(req.url === "/archiveCourse" && req.method === "DELETE") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.write(`Archive course in our resources`);
		res.end();
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write(`PAGE NOT FOUND`);
		res.end();
	}

});
server.listen(PORT, () => console.log(`Server running smoothly on localhost:${PORT}`));